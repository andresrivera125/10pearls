import React, { Component } from 'react';
import { Header, Icon, Divider, Container } from 'semantic-ui-react';
import TableClient from './components/clients/TableClient';
import Menu from './components/Menu';
import Reports from './components/reports';
import './App.css';

const CLIENTS = 'clients';
const REPORTS = 'reports';

class App extends Component {
  constructor(){
    super();

    this.state = {
      actualMenu: CLIENTS,
    }

    this.selectMenu = this.selectMenu.bind(this);
  }

  selectMenu(menu){
    this.setState({actualMenu: menu});
  }

  renderMenu(actualMenu){
    let retorno = '';
    if(actualMenu === CLIENTS){
      retorno = <TableClient />
    }else{
      retorno = <Reports />
    }

    return retorno;
  }

  render(){
    const {actualMenu} = this.state;
    return (
      <div className="App">
        <Header as='h2' icon textAlign='center'>
          <Icon name='users' circular />
          <Header.Content>InfoClients</Header.Content>
        </Header>
        <Divider/>
        <Container textAlign='center'>
          <Menu selectMenu={this.selectMenu} actualMenu={actualMenu} CLIENTS={CLIENTS} REPORTS={REPORTS}/>
          <Divider/>
          {this.renderMenu(actualMenu)}
        </Container>
      </div>
    );
  }
}

export default App;
