import React from 'react';
import { Form, Input, TextArea, Select } from 'semantic-ui-react';

function FormNewClient(props) {
    const {changeData, countries, states, cities} = props;
    const mapear = e => {return({ key: e.id, text: e.name, value: e.id})};
    const selectCountries = countries.map( mapear );
    const selectStates = states.map( mapear );
    const selectCities = cities.map( mapear );
    return (
        <Form>
            <Form.Field
                id='form-textarea-data'
                control={TextArea}
                label='Data'
                placeholder='Description of the client'
                name='data'
                onChange={(e, {name, value}) => changeData(name, value)}
            />
            <Form.Group widths='equal'>
            <Form.Field
                id='form-input-nit'
                control={Input}
                label='Nit'
                placeholder='Nit'
                name='nit'
                onChange={(e, {name, value}) => changeData(name, value)}
            />
            <Form.Field
                id='form-input-fullname'
                control={Input}
                label='Full Name'
                placeholder='Full name'
                name='fullName'
                onChange={(e, {name, value}) => changeData(name, value)}
            />
            </Form.Group>
            <Form.Group widths='equal'>
                <Form.Field
                    id='form-input-address'
                    control={Input}
                    label='Address'
                    placeholder='Address'
                    name='address'
                    onChange={(e, {name, value}) => changeData(name, value)}
                />
                <Form.Field
                    id='form-input-phone'
                    control={Input}
                    label='Phone'
                    placeholder='Phone'
                    name='phone'
                    onChange={(e, {name, value}) => changeData(name, value)}
                />
            </Form.Group>
            <Form.Group widths='equal'>
                <Form.Field
                control={Select}
                options={selectCities}
                label={{ children: 'City', htmlFor: 'form-select-control-city' }}
                placeholder='City'
                name='city'
                onChange={(e, {name, value}) => changeData(name, value)}
                search
                searchInput={{ id: 'form-select-control-city' }}
                />
                <Form.Field
                control={Select}
                options={selectStates}
                label={{ children: 'State', htmlFor: 'form-select-control-state' }}
                placeholder='State'
                search
                searchInput={{ id: 'form-select-control-state' }}
                />
                <Form.Field
                control={Select}
                options={selectCountries}
                label={{ children: 'Country', htmlFor: 'form-select-control-country' }}
                placeholder='Country'
                search
                searchInput={{ id: 'form-select-control-country' }}
                />
            </Form.Group>
            <Form.Group widths='equal'>
            <Form.Field
                    id='form-input-credit-limit'
                    control={Input}
                    label='Credit Limit'
                    placeholder='Credit Limit'
                    name='creditLimit'
                    onChange={(e, {name, value}) => changeData(name, value)}
                />
                <Form.Field
                    id='form-input-available-credit'
                    control={Input}
                    label='Available credit'
                    placeholder='Available credit'
                    name='availableCredit'
                    onChange={(e, {name, value}) => changeData(name, value)}
                />
                <Form.Field
                    id='form-input-visits-percentage'
                    control={Input}
                    label='Visits percentage'
                    placeholder='Visits percentage'
                    name='visitsPercentage'
                    onChange={(e, {name, value}) => changeData(name, value)}
                />
            </Form.Group>
        </Form>
    );
}

export default FormNewClient;