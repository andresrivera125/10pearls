import React, {Component} from 'react'
import { Button, Modal, Icon } from 'semantic-ui-react';
import FormUpdateClient from './FormUpdateClient';

class ModalUpdate extends Component {
    constructor(props){
        super(props);
        this.state= {
            modalOpen: false,
        };

        this.data = this.initInformation(this.props.client);
        this.changeData = this.changeData.bind(this);
    }

    handleOpen = () => {
        this.setState({ modalOpen: true });
    }

    handleClose = () => this.setState({ modalOpen: false });

    changeData(field, value) {
        this.data[field] = value;
    }

    initInformation(client){
        console.log(client);
        const data = {
            clientId: client.clientId,
            data: client.data,
            creditLimit: client.creditLimit,
            availableCredit: client.availableCredit,
            visitsPercentage: client.visitsPercentage,
            nit: client.personDTO.nit,
            fullName: client.personDTO.fullName,
            address: client.personDTO.address,
            phone: client.personDTO.phone,
            city: client.personDTO.cityDTO.id,
            state: client.personDTO.cityDTO.stateDTO.id,
            country: client.personDTO.cityDTO.stateDTO.countryDTO.id,
        };

        return data;
    }

    updateClient = () => {
        console.log(this.data);
        const data = this.data;
        const info = {
            data: data.data,
            personDTO: {
                nit: data.nit,
                fullName: data.fullName,
                address: data.address,
                phone: data.phone,
                cityDTO: {
                    id: data.city,
                },
            },
            creditLimit: data.creditLimit,
            availableCredit: data.availableCredit,
            visitsPercentage: data.visitsPercentage,
        };

        console.log(JSON.stringify(info));
    }

    render() {
        const {countries, states, cities} = this.props;
        return (
            <Modal trigger={<Button icon color='blue' size='mini' onClick={this.handleOpen}>
                        <Icon name='pencil'/>
                    </Button>}
                open={this.state.modalOpen}
                onClose={this.handleClose}
                size='large'
            >
                <Modal.Header>New Client</Modal.Header>
                <Modal.Content >
                <Modal.Description>
                    <FormUpdateClient initData={this.data} changeData={this.changeData} countries={countries} states={states} cities={cities}/>
                </Modal.Description>
                </Modal.Content>
                <Modal.Actions>
                <Button color='green' onClick={this.insertClient}>
                    <Icon name='checkmark' /> Save
                </Button>
                <Button basic color='red' onClick={this.handleClose}>
                    <Icon name='remove' /> Cancel
                </Button>
                </Modal.Actions>
            </Modal>
        );
    }
}

export default ModalUpdate