import React, { Component } from 'react';
import { Icon, Table, Message } from 'semantic-ui-react';
import TableHeader from './TableHeader';
import RowClient from './RowClient';

const api = 'http://localhost:8000/infoclients/';
const apiGetClients = api + 'client';
const apiGetCountries = api + 'country';
const apiGetStates = api + 'state';
const apiGetCities = api + 'city';

const ERROR_CLIENTS = '';
const ERROR_CITIES = '';
const ERROR_STATES = '';
const ERROR_COUNTRIES = '';

class TableClient extends Component {

  constructor(){
    super();
    this.state = {
      clients: [],
      cities: [],
      states: [],
      countries: [],
      loadError: false,
      messageErrorUser: '',
      messageErrorSystem: '',
    }
  }

  componentDidMount(){
    this.fetchClients();
    this.fetchCountries();
    this.fetchStates();
    this.fetchCities();
  }

  getFetch(url, callback, messageUser){
    const headers = new Headers();

    var params = { method: 'GET',
                  headers: headers,
                  mode: 'cors',
                  cache: 'default' };
    fetch(url, params)
      .then( response => response.json() )
      .then(rs => callback(rs) )
      .catch( error => this.setState({ loadError: true,
        messageErrorUser: messageUser, 
        messageErrorSystem: error.message })
        );
  }

  fetchClients = () => this.getFetch(apiGetClients, this.getClients, ERROR_CLIENTS); 
  fetchCountries = () => this.getFetch(apiGetCountries, this.getCountries, ERROR_COUNTRIES);
  fetchStates = () => this.getFetch(apiGetStates, this.getStates, ERROR_STATES);
  fetchCities = () => this.getFetch(apiGetCities, this.getCities, ERROR_CITIES);

  getClients = (clients) => {this.setState(prev => {return {clients}; });}
  getCountries = (countries) => {this.setState(prev => {return {countries}; });}
  getStates = (states) => {this.setState(prev => {return {states}; });}
  getCities = (cities) => {this.setState(prev => {return {cities}; });}

  render() {
    const {clients, countries, states, cities, loadError, messageErrorUser, messageErrorSystem} = this.state;
    let retorno = '';

    if(clients.length && countries.length && states.length && cities.length){
      retorno = (
        <div>
          <Table fixed>
            <Table.Header>
              <TableHeader countries={countries} states={states} cities={cities}/>
            </Table.Header>

            <Table.Body>
              {clients.map( client => <RowClient key={client.idClient} client={client} countries={countries} states={states} cities={cities}/>)}
            </Table.Body>
          </Table>
        </div>
      );
    } else if(!loadError) {
      retorno = (
        <Message icon>
          <Icon name='circle notched' loading />
          <Message.Content>
            <Message.Header>Just one second</Message.Header>
            We are fetching the client information.
          </Message.Content>
        </Message>
      );
    } else {
      retorno = (
        <Message negative>
          <Message.Header>{messageErrorUser}</Message.Header>
          <p>Please contact the administrator</p>
        </Message>
      );

      console.error('A problem ocurre while we try to consume an api.  This is the error message: ', messageErrorSystem);
    }
    return retorno;
  }
}

export default TableClient;