import React, {Component} from 'react';
import { Icon, Table, Button } from 'semantic-ui-react';
import ModalUpdate from './ModalUpdate';
import ModalVisit from '../visits/ModalVisit';

class RowClient extends Component {

    render() {
        const {client, countries, states, cities} = this.props;

        return (
            <Table.Row className='table-body'>
                <Table.Cell>{client.data}</Table.Cell>
                <Table.Cell textAlign='right'>{client.personDTO.nit}</Table.Cell>
                <Table.Cell>{client.personDTO.fullName}</Table.Cell>
                <Table.Cell>{client.personDTO.address}</Table.Cell>
                <Table.Cell>{client.personDTO.phone}</Table.Cell>
                <Table.Cell>{client.personDTO.cityDTO.name}</Table.Cell>
                <Table.Cell>{client.personDTO.cityDTO.stateDTO.name}</Table.Cell>
                <Table.Cell>{client.personDTO.cityDTO.stateDTO.countryDTO.name}</Table.Cell>
                <Table.Cell textAlign='right'>{client.creditLimit}</Table.Cell>
                <Table.Cell textAlign='right'>{client.availableCredit}</Table.Cell>
                <Table.Cell textAlign='right'>{client.visitsPercentage}</Table.Cell>
                <Table.Cell textAlign='center'>
                    <ModalUpdate client={client} countries={countries} states={states} cities={cities}/>
                    <ModalVisit visits={client.visitDTOList} />
                    <Button icon color='red' size='mini'><Icon name='trash'/></Button>
                </Table.Cell>
            </Table.Row>
        );
    }
}

export default RowClient;