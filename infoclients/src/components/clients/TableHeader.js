import React from 'react';
import { Table} from 'semantic-ui-react';
import ModalInsert from './ModalInsert';

function TableHeader(props) {
    const {countries, states, cities} = props;
    return (
        <Table.Row className="table-header">
              <Table.HeaderCell width='2'>Data</Table.HeaderCell>
              <Table.HeaderCell>Nit</Table.HeaderCell>
              <Table.HeaderCell>Full name</Table.HeaderCell>
              <Table.HeaderCell>Address</Table.HeaderCell>
              <Table.HeaderCell>Phone</Table.HeaderCell>
              <Table.HeaderCell>City</Table.HeaderCell>
              <Table.HeaderCell>State</Table.HeaderCell>
              <Table.HeaderCell>Country</Table.HeaderCell>
              <Table.HeaderCell>Credit limit</Table.HeaderCell>
              <Table.HeaderCell>Available credit</Table.HeaderCell>
              <Table.HeaderCell>Visits percentage</Table.HeaderCell>
              <Table.HeaderCell textAlign='center' width='2'>
                <ModalInsert countries={countries} states={states} cities={cities}/>
              </Table.HeaderCell>
            </Table.Row>
    );
}

export default TableHeader;