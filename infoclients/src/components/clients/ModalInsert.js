import React, {Component} from 'react'
import { Button, Modal, Icon } from 'semantic-ui-react';
import FormNewClient from './FormNewClient';

const CLIENT_INSERTED = 'The client was inserted';
const CLIENT_NO_INSERTED = 'It was not possible to insert the new client.  Please contact the administrator';

class ModalInsert extends Component {
    constructor(props){
        super(props);
        this.state= {
            modalOpen: false,
            fetchInsert: false,
            fetchError: false,
            messageErrorUser: '',
        };

        this.data = {};
        this.changeData = this.changeData.bind(this);
    }

    handleOpen = () => this.setState({ modalOpen: true });

    handleClose = () => this.setState({ modalOpen: false });

    changeData(field, value) {
        this.data[field] = value;
    }

    insertClient = () => {
        console.log(this.data);
        const data = this.data;
        const info = {
            data: data.data,
            personDTO: {
                nit: data.nit,
                fullName: data.fullName,
                address: data.address,
                phone: data.phone,
                cityDTO: {
                    id: data.city,
                },
            },
            creditLimit: data.creditLimit,
            availableCredit: data.availableCredit,
            visitsPercentage: data.visitsPercentage,
        };

        const dataJson = JSON.stringify(info);

        this.setFetch('POST', data, CLIENT_INSERTED, CLIENT_NO_INSERTED);
    }

    setFetch(method, data, messageOk, messageError){
        const urlApi = 'http://localhost:8000/infoclients/client';
        const params = { method: method,
                      body: data,
                      headers: {'Content-Type': 'application/json'},
                      mode: 'cors',
                      cache: 'default' };
        fetch(urlApi, params)
          .then( response => response.json() )
          .then(rs => messageOk(messageOk, rs) )
          .catch( error => messageError(messageErrorUser, messageErrorSystem, error) );
      }
    }

    messageOk(message, rs){
        this.setState( prev => {
            prev.fetchInsert = true,
            prev.messageOk = message,
        });
    }
    
    messageError(errorUser, errorSystem, rs){
        console.error(messageErrorSystem, rs);
        this.setState( prev => {
            prev.fetchError = true,
            prev.messageErrorUser = errorUser,
        });
    }

    render() {
        const {countries, states, cities} = this.props;
        return (
            <Modal trigger={<Button icon color='green' size='mini' onClick={this.handleOpen}>
                        <Icon name='add'/>
                    </Button>}
                open={this.state.modalOpen}
                onClose={this.handleClose}
                size='large'
            >
                <Modal.Header>New Client</Modal.Header>
                <Modal.Content >
                <Modal.Description>
                    <FormNewClient changeData={this.changeData} countries={countries} states={states} cities={cities}/>
                </Modal.Description>
                </Modal.Content>
                <Modal.Actions>
                <Button color='green' onClick={this.insertClient}>
                    <Icon name='checkmark' /> Save
                </Button>
                <Button basic color='red' onClick={this.handleClose}>
                    <Icon name='remove' /> Cancel
                </Button>
                </Modal.Actions>
            </Modal>
        );
    }
}

export default ModalInsert