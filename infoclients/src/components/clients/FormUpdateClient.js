import React from 'react';
import { Form, Input, TextArea, Select } from 'semantic-ui-react';

function FormUpdateClient(props) {
    const {changeData, countries, states, cities, initData} = props;
    const mapear = e => {return({ key: e.id, text: e.name, value: e.id})};
    const selectCountries = countries.map( mapear );
    const selectStates = states.map( mapear );
    const selectCities = cities.map( mapear );
    return (
        <Form>
            <Form.Field
                id='form-textarea-data'
                control={TextArea}
                label='Data'
                placeholder='Description of the client'
                name='data'
                value={initData.data}
                onChange={(e, {name, value}) => changeData(name, value)}
            />
            <Form.Group widths='equal'>
            <Form.Field
                id='form-input-nit'
                control={Input}
                label='Nit'
                placeholder='Nit'
                name='nit'
                value={initData.nit}
                onChange={(e, {name, value}) => changeData(name, value)}
            />
            <Form.Field
                id='form-input-fullname'
                control={Input}
                label='Full Name'
                placeholder='Full name'
                name='fullName'
                value={initData.fullName}
                onChange={(e, {name, value}) => changeData(name, value)}
            />
            </Form.Group>
            <Form.Group widths='equal'>
                <Form.Field
                    id='form-input-address'
                    control={Input}
                    label='Address'
                    placeholder='Address'
                    name='address'
                    value={initData.address}
                    onChange={(e, {name, value}) => changeData(name, value)}
                />
                <Form.Field
                    id='form-input-phone'
                    control={Input}
                    label='Phone'
                    placeholder='Phone'
                    name='phone'
                    value={initData.phone}
                    onChange={(e, {name, value}) => changeData(name, value)}
                />
            </Form.Group>
            <Form.Group widths='equal'>
                <Form.Field
                control={Select}
                options={selectCities}
                label={{ children: 'City', htmlFor: 'form-select-control-city' }}
                placeholder='City'
                name='city'
                value={initData.city}
                onChange={(e, {name, value}) => changeData(name, value)}
                search
                searchInput={{ id: 'form-select-control-city' }}
                />
                <Form.Field
                control={Select}
                options={selectStates}
                label={{ children: 'State', htmlFor: 'form-select-control-state' }}
                placeholder='State'
                value={initData.state}
                search
                searchInput={{ id: 'form-select-control-state' }}
                />
                <Form.Field
                control={Select}
                options={selectCountries}
                label={{ children: 'Country', htmlFor: 'form-select-control-country' }}
                placeholder='Country'
                value={initData.country}
                search
                searchInput={{ id: 'form-select-control-country' }}
                />
            </Form.Group>
            <Form.Group widths='equal'>
            <Form.Field
                    id='form-input-credit-limit'
                    control={Input}
                    label='Credit Limit'
                    placeholder='Credit Limit'
                    name='creditLimit'
                    value={initData.creditLimit}
                    onChange={(e, {name, value}) => changeData(name, value)}
                />
                <Form.Field
                    id='form-input-available-credit'
                    control={Input}
                    label='Available credit'
                    placeholder='Available credit'
                    name='availableCredit'
                    value={initData.availableCredit}
                    onChange={(e, {name, value}) => changeData(name, value)}
                />
                <Form.Field
                    id='form-input-visits-percentage'
                    control={Input}
                    label='Visits percentage'
                    placeholder='Visits percentage'
                    name='visitsPercentage'
                    value={initData.visitsPercentage}
                    onChange={(e, {name, value}) => changeData(name, value)}
                />
            </Form.Group>
        </Form>
    );
}

export default FormUpdateClient;