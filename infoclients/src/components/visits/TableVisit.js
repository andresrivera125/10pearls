import React from 'react';
import { Icon, Table, Button } from 'semantic-ui-react';
import RowVisit from './RowVisit';

function TableVisit(props) {
  return (
    <Table fixed>
      <Table.Header>
        <Table.Row className="table-header">
          <Table.HeaderCell width='2'>Sales representative</Table.HeaderCell>
          <Table.HeaderCell>Date</Table.HeaderCell>
          <Table.HeaderCell>City</Table.HeaderCell>
          <Table.HeaderCell>Net</Table.HeaderCell>
          <Table.HeaderCell>Visit total</Table.HeaderCell>
          <Table.HeaderCell>Description</Table.HeaderCell>
          <Table.HeaderCell textAlign='center' width='2'>
            <Button icon color='green' size='mini'>
              <Icon name='add'/>
            </Button>
          </Table.HeaderCell>
        </Table.Row>
      </Table.Header>

      <Table.Body>
        <RowVisit />
      </Table.Body>
    </Table>
  );
}

export default TableVisit;