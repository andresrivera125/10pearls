import React from 'react';
import { Icon, Table, Button } from 'semantic-ui-react';

function RowVisit() {
    return (
        <Table.Row className='table-body'>
            <Table.Cell>He is a familiar person</Table.Cell>
            <Table.Cell textAlign='right'>1</Table.Cell>
            <Table.Cell>Client 34</Table.Cell>
            <Table.Cell>Florida street</Table.Cell>
            <Table.Cell>555-55-55</Table.Cell>
            <Table.Cell>Miami</Table.Cell>
            <Table.Cell>Florida</Table.Cell>
            <Table.Cell>United States of America</Table.Cell>
            <Table.Cell textAlign='right'>100.000</Table.Cell>
            <Table.Cell textAlign='right'>90.000</Table.Cell>
            <Table.Cell textAlign='right'>2</Table.Cell>
            <Table.Cell textAlign='center'>
            <Button icon color='blue' size='mini'><Icon name='pencil'/></Button>
            <Button icon color='blue' size='mini'><Icon name='book'/></Button>
            <Button icon color='red' size='mini'><Icon name='trash'/></Button>
            </Table.Cell>
        </Table.Row>
    );
}

export default RowVisit;