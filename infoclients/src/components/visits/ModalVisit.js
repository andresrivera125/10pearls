import React, {Component} from 'react'
import { Button, Modal, Icon } from 'semantic-ui-react';
import TableVisit from './TableVisit';

class ModalVisit extends Component {
    constructor(props){
        super(props);
        this.state= {
            modalOpen: false,
        };
    }

    handleOpen = () => {
        this.setState({ modalOpen: true });
    }

    handleClose = () => this.setState({ modalOpen: false });

    render() {
        const {visits} = this.props;
        return (
            <Modal trigger={<Button icon color='blue' size='mini' onClick={this.handleOpen}>
                        <Icon name='book'/>
                    </Button>}
                open={this.state.modalOpen}
                onClose={this.handleClose}
                size='large'
            >
                <Modal.Header>Visit's log</Modal.Header>
                <Modal.Content >
                <Modal.Description>
                    <TableVisit visits={visits}/>
                </Modal.Description>
                </Modal.Content>
                <Modal.Actions>
                <Button color='green' onClick={this.insertClient}>
                    <Icon name='checkmark' /> Save
                </Button>
                <Button basic color='red' onClick={this.handleClose}>
                    <Icon name='remove' /> Cancel
                </Button>
                </Modal.Actions>
            </Modal>
        );
    }
}

export default ModalVisit