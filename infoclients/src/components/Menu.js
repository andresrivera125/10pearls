import React from 'react';
import { Button } from 'semantic-ui-react';

function Menu(props){
    const {selectMenu, actualMenu, CLIENTS, REPORTS} = props;
    let clientsIsSelect = true;
    let reportsIsSelect = false;

    switch(actualMenu){
        case CLIENTS:
            clientsIsSelect = true;
            reportsIsSelect = false;
            break;
        case REPORTS:
            reportsIsSelect = true;
            clientsIsSelect = false;
            break;
        default:
            clientsIsSelect = true;
            reportsIsSelect = false;
    }

    return (
        <Button.Group>
          <Button color='blue' positive={clientsIsSelect} onClick={() => selectMenu(CLIENTS)} >Clients</Button>
          <Button.Or />
          <Button positive={reportsIsSelect} onClick={() => selectMenu(REPORTS)}>Reports</Button>
        </Button.Group>
    );
}

export default Menu;