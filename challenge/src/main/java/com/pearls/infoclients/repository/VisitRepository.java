package com.pearls.infoclients.repository;

import com.pearls.infoclients.model.Visit;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface VisitRepository extends CrudRepository<Visit, Long> {

    public List<Visit> findByClientId(Long idClient);

    @Query("select city.name, count(*) from visit join city on city.id = visit.city_id group by city.name")
    public List<Visit> findAllGroupByCityName();
}
