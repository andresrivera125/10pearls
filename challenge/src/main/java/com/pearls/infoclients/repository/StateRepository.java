package com.pearls.infoclients.repository;

import com.pearls.infoclients.model.State;
import org.springframework.data.repository.CrudRepository;

public interface StateRepository extends CrudRepository<State, Long> {

}
