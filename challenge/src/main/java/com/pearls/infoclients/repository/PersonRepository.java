package com.pearls.infoclients.repository;

import com.pearls.infoclients.dto.PersonDTO;
import com.pearls.infoclients.model.Person;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PersonRepository extends CrudRepository<Person, Long> {

}
