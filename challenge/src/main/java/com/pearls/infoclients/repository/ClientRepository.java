package com.pearls.infoclients.repository;

import com.pearls.infoclients.model.Client;
import org.springframework.data.repository.CrudRepository;

public interface ClientRepository extends CrudRepository<Client, Long> {

}
