package com.pearls.infoclients.repository;

import com.pearls.infoclients.model.SalesRepresentative;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SalesRepresentativeRepository extends CrudRepository<SalesRepresentative, Long> {

    public List<SalesRepresentative> findByCityId(Long idCity);

}
