package com.pearls.infoclients.repository;

import com.pearls.infoclients.model.Country;
import org.springframework.data.repository.CrudRepository;

public interface CountryRepository extends CrudRepository<Country, Long> {

}
