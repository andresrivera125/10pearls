package com.pearls.infoclients.repository;

import com.pearls.infoclients.model.City;
import org.springframework.data.repository.CrudRepository;

public interface CityRepository extends CrudRepository<City, Long> {

}
