package com.pearls.infoclients.service;

import com.pearls.infoclients.dto.VisitDTO;
import com.pearls.infoclients.model.Visit;
import com.pearls.infoclients.repository.VisitRepository;
import com.pearls.infoclients.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class VisitServiceImpl implements VisitService {

    @Autowired
    private VisitRepository visitRepository;

    @Override
    public List<VisitDTO> findByClientId(Long clientId) {
        List<VisitDTO> visitDTOList = new ArrayList<>();
        visitRepository.findByClientId(clientId)
                .forEach(e -> visitDTOList.add(new VisitDTO(e)));
        return visitDTOList;
    }

    @Override
    public List<VisitDTO> findAllGroupByCityName() {
        List<VisitDTO> visitDTOList = new ArrayList<>();
        visitRepository.findAllGroupByCityName()
                .forEach(e -> visitDTOList.add(new VisitDTO(e)));
        return visitDTOList;
    }

    @Override
    public VisitDTO insert(VisitDTO visitDTO) throws InsertVisitException {
        Visit visit = visitDTO.toVisit();
        visit.setId(null);

        VisitDTO newVisitDTO = new VisitDTO(visitRepository.save(visit));

        return newVisitDTO;
    }

    @Override
    public VisitDTO update(VisitDTO visitDTO, Long id) throws UpdateVisitException {
        Visit visit = visitDTO.toVisit();
        visit.setId(id);

        VisitDTO newVisitDTO = new VisitDTO(visitRepository.save(visit));

        return newVisitDTO;
    }

    @Override
    public void delete(Long id) throws DeleteVisitException {
        visitRepository.deleteById(id);
    }

    @Override
    public void deleteAll(List<VisitDTO> visitDTOList) throws DeleteVisitException {
        Iterable<Visit> visitIterable = visitDTOList.stream().map(e -> e.toVisit()).collect(Collectors.toList());
        visitRepository.deleteAll(visitIterable);
    }

    @Override
    public void saveAll(List<VisitDTO> visitDTOList) throws UpdateVisitException {
        Iterable<Visit> visitIterable = visitDTOList.stream().map(e -> e.toVisit()).collect(Collectors.toList());
        visitRepository.saveAll(visitIterable);
    }
}
