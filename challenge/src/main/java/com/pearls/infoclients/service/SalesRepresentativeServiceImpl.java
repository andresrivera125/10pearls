package com.pearls.infoclients.service;

import com.pearls.infoclients.dto.SalesRepresentativeDTO;
import com.pearls.infoclients.repository.SalesRepresentativeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SalesRepresentativeServiceImpl implements SalesRepresentativeService {

    @Autowired
    private SalesRepresentativeRepository salesRepresentativeRepository;

    @Override
    public List<SalesRepresentativeDTO> findByCityId(Long idCity) {
        List<SalesRepresentativeDTO> salesRepresentativeDTOList = new ArrayList<>();
        salesRepresentativeRepository.findByCityId(idCity)
                .forEach(e -> salesRepresentativeDTOList.add(new SalesRepresentativeDTO(e)));
        return salesRepresentativeDTOList;
    }
}
