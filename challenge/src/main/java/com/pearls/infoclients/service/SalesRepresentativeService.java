package com.pearls.infoclients.service;

import com.pearls.infoclients.dto.SalesRepresentativeDTO;

import java.util.List;

public interface SalesRepresentativeService {

    public List<SalesRepresentativeDTO> findByCityId(Long idCity);
}
