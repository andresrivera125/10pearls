package com.pearls.infoclients.service;

import com.pearls.infoclients.dto.PersonDTO;
import com.pearls.infoclients.model.Person;
import com.pearls.infoclients.repository.PersonRepository;
import com.pearls.infoclients.util.DeletePersonException;
import com.pearls.infoclients.util.InsertPersonException;
import com.pearls.infoclients.util.UpdatePersonException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PersonServiceImpl implements PersonService {

    @Autowired
    private PersonRepository personRepository;

    @Override
    public List<PersonDTO> findAll() {
        List<PersonDTO> personDTOList = new ArrayList<>();
        personRepository.findAll()
                .forEach(e -> personDTOList.add(new PersonDTO(e)));
        return personDTOList;
    }

    @Override
    public PersonDTO findByID(Long id) {
        PersonDTO personDTO = new PersonDTO(personRepository.findById(id).get());

        return personDTO;
    }

    @Override
    public PersonDTO insert(PersonDTO personDTO) throws InsertPersonException {
        Person person = personDTO.toPerson();
        person.setId(null);

        PersonDTO newPersonDTO = new PersonDTO(personRepository.save(person));

        return newPersonDTO;
    }

    @Override
    public PersonDTO update(PersonDTO personDTO, Long id) throws UpdatePersonException {
        Person person = personDTO.toPerson();
        person.setId(id);

        PersonDTO newPersonDTO = new PersonDTO(personRepository.save(person));

        return newPersonDTO;
    }

    @Override
    public void delete(Long id) throws DeletePersonException {
        personRepository.deleteById(id);
    }
}
