package com.pearls.infoclients.service;

import com.pearls.infoclients.dto.CountryDTO;

import java.util.List;

public interface CountryService {

    public List<CountryDTO> findAll();
}
