package com.pearls.infoclients.service;

import com.pearls.infoclients.dto.StateDTO;

import java.util.List;

public interface StateService {

    public List<StateDTO> findAll();
}
