package com.pearls.infoclients.service;

import com.pearls.infoclients.dto.PersonDTO;
import com.pearls.infoclients.util.DeletePersonException;
import com.pearls.infoclients.util.InsertPersonException;
import com.pearls.infoclients.util.UpdatePersonException;

import java.util.List;

public interface PersonService {
    public List<PersonDTO> findAll();

    public PersonDTO findByID(Long id);

    public PersonDTO insert(PersonDTO personDTO) throws InsertPersonException;

    public PersonDTO update(PersonDTO personDTO, Long id) throws UpdatePersonException;

    public void delete(Long id) throws DeletePersonException;
}
