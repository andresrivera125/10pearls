package com.pearls.infoclients.service;

import com.pearls.infoclients.dto.CityDTO;
import com.pearls.infoclients.repository.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CityServiceImpl implements CityService {

    @Autowired
    private CityRepository cityRepository;

    @Override
    public List<CityDTO> findAll() {
        List<CityDTO> cityDTOList = new ArrayList<>();

        cityRepository.findAll()
                .forEach(e -> cityDTOList.add(new CityDTO(e)));

        return cityDTOList;
    }
}
