package com.pearls.infoclients.service;

import com.pearls.infoclients.dto.VisitDTO;
import com.pearls.infoclients.util.*;

import java.util.List;

public interface VisitService {

    public List<VisitDTO> findByClientId(Long clientId);

    public List<VisitDTO> findAllGroupByCityName();

    public VisitDTO insert(VisitDTO visitDTO) throws InsertVisitException;

    public VisitDTO update(VisitDTO visitDTO, Long id) throws UpdateVisitException;

    public void delete(Long id) throws DeleteVisitException;

    public void deleteAll(List<VisitDTO> visitDTOList) throws DeleteVisitException;

    public void saveAll(List<VisitDTO> visitDTOList) throws UpdateVisitException;
}
