package com.pearls.infoclients.service;

import com.pearls.infoclients.dto.CountryDTO;
import com.pearls.infoclients.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CountryServiceImpl implements CountryService {

    @Autowired
    private CountryRepository countryRepository;

    @Override
    public List<CountryDTO> findAll() {
        List<CountryDTO> countryDTOList = new ArrayList<>();

        countryRepository.findAll()
                .forEach(e -> countryDTOList.add(new CountryDTO(e)));

        return countryDTOList;
    }
}
