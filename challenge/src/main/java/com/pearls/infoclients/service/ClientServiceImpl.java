package com.pearls.infoclients.service;

import com.pearls.infoclients.dto.ClientDTO;
import com.pearls.infoclients.dto.VisitDTO;
import com.pearls.infoclients.model.Client;
import com.pearls.infoclients.repository.ClientRepository;
import com.pearls.infoclients.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ClientServiceImpl implements ClientService {

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    protected VisitService visitService;

    @Override
    public List<ClientDTO> findAll() {
        List<ClientDTO> clientDTOList = new ArrayList<>();
        clientRepository.findAll()
                .forEach(e -> {
                    ClientDTO client = new ClientDTO(e);
                    Long idClient = client.getIdClient();
                    List<VisitDTO> visitDTOList = visitService.findByClientId(idClient);
                    client.setVisitDTOList(visitDTOList);
                    clientDTOList.add(client);

                });
        return clientDTOList;
    }

    @Override
    public ClientDTO insert(ClientDTO clientDTO) throws InsertClientException {
        Client client = clientDTO.toClient();
        client.setId(null);

        ClientDTO newClientDTO = new ClientDTO(clientRepository.save(client));

        return newClientDTO;
    }

    @Override
    public ClientDTO update(ClientDTO clientDTO, Long id) throws UpdateClientException {
        Client client = clientDTO.toClient();
        client.setId(id);

        ClientDTO newClientDTO = new ClientDTO(clientRepository.save(client));

        return newClientDTO;
    }

    @Override
    public void delete(Long id) throws DeleteClientException {
        clientRepository.deleteById(id);
    }
}
