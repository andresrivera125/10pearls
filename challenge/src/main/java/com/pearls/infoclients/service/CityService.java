package com.pearls.infoclients.service;

import com.pearls.infoclients.dto.CityDTO;

import java.util.List;

public interface CityService {

    public List<CityDTO> findAll();
}
