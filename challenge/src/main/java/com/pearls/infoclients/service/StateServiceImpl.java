package com.pearls.infoclients.service;

import com.pearls.infoclients.dto.StateDTO;
import com.pearls.infoclients.repository.StateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StateServiceImpl implements StateService {

    @Autowired
    private StateRepository stateRepository;

    @Override
    public List<StateDTO> findAll() {
        List<StateDTO> stateDTOList = new ArrayList<>();

        stateRepository.findAll()
                .forEach(e -> stateDTOList.add(new StateDTO(e)));

        return stateDTOList;
    }
}
