package com.pearls.infoclients.service;

import com.pearls.infoclients.dto.ClientDTO;
import com.pearls.infoclients.dto.PersonDTO;
import com.pearls.infoclients.util.DeleteClientException;
import com.pearls.infoclients.util.InsertClientException;
import com.pearls.infoclients.util.UpdateClientException;

import java.util.List;

public interface ClientService {
    public List<ClientDTO> findAll();

    public ClientDTO insert(ClientDTO clientDTO) throws InsertClientException;

    public ClientDTO update(ClientDTO clientDTO, Long id) throws UpdateClientException;

    public void delete(Long id) throws DeleteClientException;
}
