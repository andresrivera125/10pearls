package com.pearls.infoclients.model;

import javax.persistence.*;

@Entity
@Table(name = "client")
public class Client {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    @OneToOne
    private Person person;

    @Column(nullable = false)
    private String data;

    @Column(nullable = false)
    private Long creditLimit;

    @Column(nullable = false)
    private Long availableCredit;

    @Column(nullable = false)
    private Long visitsPercentage;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Long getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(Long creditLimit) {
        this.creditLimit = creditLimit;
    }

    public Long getAvailableCredit() {
        return availableCredit;
    }

    public void setAvailableCredit(Long availableCredit) {
        this.availableCredit = availableCredit;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Long getVisitsPercentage() {
        return visitsPercentage;
    }

    public void setVisitsPercentage(Long visitsPercentage) {
        this.visitsPercentage = visitsPercentage;
    }
}
