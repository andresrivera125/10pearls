package com.pearls.infoclients.dto;

import com.pearls.infoclients.model.State;

import java.io.Serializable;

public class StateDTO implements Serializable {
    private static final long serialVersionUID = -7083716669713716289L;

    private Long id;
    private String name;
    private CountryDTO countryDTO;

    public StateDTO(State state) {
        this.id = state.getId();
        this.name = state.getName();
        this.countryDTO = new CountryDTO(state.getCountry());
    }

    public State toState(){
        State state = new State();
        state.setId(this.getId());
        state.setName(this.getName());
        state.setCountry(this.getCountryDTO().toCountry());

        return state;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CountryDTO getCountryDTO() {
        return countryDTO;
    }

    public void setCountryDTO(CountryDTO countryDTO) {
        this.countryDTO = countryDTO;
    }
}
