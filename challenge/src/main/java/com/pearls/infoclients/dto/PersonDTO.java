package com.pearls.infoclients.dto;

import com.pearls.infoclients.model.City;
import com.pearls.infoclients.model.Person;

import java.io.Serializable;
import java.util.Base64;

public class PersonDTO implements Serializable {

    private static final long serialVersionUID = -8680615478246941496L;

    private Long idPerson;
    private String fullName;
    private String nit;
    private String address;
    private String phone;
    private CityDTO cityDTO;

    public PersonDTO(Person person) {
        this.idPerson = person.getId();
        this.fullName = person.getFullname();
        this.nit = person.getNit();
        this.address = person.getAddress();
        this.phone = person.getPhone();
        this.cityDTO = new CityDTO(person.getCity());
    }

    public Person toPerson(){
        Person person = new Person();
        person.setId(this.getIdPerson());
        person.setFullname(this.getFullName());
        person.setNit(this.getNit());
        person.setAddress(this.getAddress());
        person.setPhone(this.getPhone());
        person.setCity(this.getCityDTO().toCity());

        return person;
    }

    public Long getIdPerson() {
        return idPerson;
    }

    public void setIdPerson(Long idPerson) {
        this.idPerson = idPerson;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getNit() {
        return Base64.getDecoder().decode(this.nit).toString();
    }

    public void setNit(String nit) {
        this.nit = Base64.getEncoder().encodeToString(nit.getBytes());
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public CityDTO getCityDTO() {
        return cityDTO;
    }

    public void setCityDTO(CityDTO cityDTO) {
        this.cityDTO = cityDTO;
    }
}
