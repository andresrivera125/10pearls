package com.pearls.infoclients.dto;

import com.pearls.infoclients.model.City;
import com.pearls.infoclients.model.SalesRepresentative;
import com.pearls.infoclients.model.Visit;

import java.io.Serializable;
import java.util.Date;

public class VisitDTO implements Serializable {
    private static final long serialVersionUID = 5645887836071268388L;

    private Long id;
    private ClientDTO clientDTO;
    private SalesRepresentativeDTO salesRepresentativeDTO;
    private Date date;
    private CityDTO cityDTO;
    private Long net;
    private String description;
    private Long visitTotal;

    public VisitDTO(Visit visit){
        this.id = visit.getId();
        this.clientDTO = new ClientDTO(visit.getClient());
        this.salesRepresentativeDTO = new SalesRepresentativeDTO(visit.getSalesRepresentative());
        this.date = visit.getDate();
        this.cityDTO = new CityDTO(visit.getCity());
        this.net = visit.getNet();
        this.description = visit.getDescription();
        setVisitTotal();
    }

    public Visit toVisit(){
        Visit visit = new Visit();
        visit.setId(this.getId());
        visit.setClient(this.getClientDTO().toClient());
        visit.setSalesRepresentative(this.getSalesRepresentativeDTO().toSalesRepresentative());
        visit.setDate(this.getDate());
        visit.setCity(this.getCityDTO().toCity());
        visit.setNet(this.getNet());
        visit.setDescription(this.getDescription());

        return visit;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ClientDTO getClientDTO() {
        return clientDTO;
    }

    public void setClientDTO(ClientDTO clientDTO) {
        this.clientDTO = clientDTO;
    }

    public SalesRepresentativeDTO getSalesRepresentativeDTO() {
        return salesRepresentativeDTO;
    }

    public void setSalesRepresentativeDTO(SalesRepresentativeDTO salesRepresentativeDTO) {
        this.salesRepresentativeDTO = salesRepresentativeDTO;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public CityDTO getCityDTO() {
        return cityDTO;
    }

    public void setCityDTO(CityDTO cityDTO) {
        this.cityDTO = cityDTO;
    }

    public Long getNet() {
        return net;
    }

    public void setNet(Long net) {
        this.net = net;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getVisitTotal() {
        return visitTotal;
    }

    private void setVisitTotal() {
        this.visitTotal = this.getNet() * this.getClientDTO().getVisitsPercentage();
    }
}
