package com.pearls.infoclients.dto;

import com.pearls.infoclients.model.Person;
import com.pearls.infoclients.model.SalesRepresentative;

import java.io.Serializable;

public class SalesRepresentativeDTO implements Serializable {

    private static final long serialVersionUID = 3636246815931239677L;

    private Long id;
    private PersonDTO personDTO;
    private CityDTO cityDTO;

    public SalesRepresentativeDTO(SalesRepresentative sales){
        this.id = sales.getId();
        this.personDTO = new PersonDTO(sales.getPerson());
        this.cityDTO = new CityDTO(sales.getCity());
    }

    public SalesRepresentative toSalesRepresentative(){
        SalesRepresentative sales = new SalesRepresentative();
        sales.setId(this.getId());
        sales.setPerson(this.getPersonDTO().toPerson());
        sales.setCity(this.getCityDTO().toCity());

        return sales;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PersonDTO getPersonDTO() {
        return personDTO;
    }

    public void setPersonDTO(PersonDTO personDTO) {
        this.personDTO = personDTO;
    }

    public CityDTO getCityDTO() {
        return cityDTO;
    }

    public void setCityDTO(CityDTO cityDTO) {
        this.cityDTO = cityDTO;
    }
}
