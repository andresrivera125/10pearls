package com.pearls.infoclients.dto;

import com.pearls.infoclients.model.Client;

import java.io.Serializable;
import java.util.List;

public class ClientDTO implements Serializable {
    private static final long serialVersionUID = -8737891592423632614L;

    private Long idClient;
    private PersonDTO personDTO;
    private String data; // A description about the client
    private Long creditLimit;
    private Long availableCredit;
    private Long visitsPercentage;
    private List<VisitDTO> visitDTOList;

    public ClientDTO(Client client) {
        this.idClient = client.getId();
        this.personDTO = new PersonDTO(client.getPerson());
        this.data = client.getData();
        this.creditLimit = client.getCreditLimit();
        this.availableCredit = client.getAvailableCredit();
        this.visitsPercentage = client.getVisitsPercentage();
    }

    public Client toClient(){
        Client client = new Client();
        client.setId(this.getIdClient());
        client.setPerson(this.getPersonDTO().toPerson());
        client.setData(this.getData());
        client.setCreditLimit(this.getCreditLimit());
        client.setAvailableCredit(this.getAvailableCredit());
        client.setVisitsPercentage(this.getVisitsPercentage());
        return client;
    }

    public Long getIdClient() {
        return idClient;
    }

    public void setIdClient(Long idClient) {
        this.idClient = idClient;
    }

    public PersonDTO getPersonDTO() {
        return personDTO;
    }

    public void setPersonDTO(PersonDTO personDTO) {
        this.personDTO = personDTO;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Long getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(Long creditLimit) {
        this.creditLimit = creditLimit;
    }

    public Long getAvailableCredit() {
        return availableCredit;
    }

    public void setAvailableCredit(Long availableCredit) {
        this.availableCredit = availableCredit;
    }

    public Long getVisitsPercentage() {
        return visitsPercentage;
    }

    public void setVisitsPercentage(Long visitsPercentage) {
        this.visitsPercentage = visitsPercentage;
    }

    public List<VisitDTO> getVisitDTOList() {
        return visitDTOList;
    }

    public void setVisitDTOList(List<VisitDTO> visitDTOList) {
        this.visitDTOList = visitDTOList;
    }
}
