package com.pearls.infoclients.dto;

import com.pearls.infoclients.model.City;

import java.io.Serializable;

public class CityDTO implements Serializable {
    private static final long serialVersionUID = -2199227565028223864L;

    private Long id;
    private String name;
    private StateDTO stateDTO;

    public CityDTO(City city) {
        this.id = city.getId();
        this.name = city.getName();
        this.stateDTO = new StateDTO(city.getState());
    }

    public City toCity(){
        City city = new City();
        city.setId(this.getId());
        city.setName(this.getName());
        city.setState(this.getStateDTO().toState());

        return city;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public StateDTO getStateDTO() {
        return stateDTO;
    }

    public void setStateDTO(StateDTO stateDTO) {
        this.stateDTO = stateDTO;
    }
}
