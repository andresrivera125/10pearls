package com.pearls.infoclients.dto;

import com.pearls.infoclients.model.Country;

import java.io.Serializable;

public class CountryDTO implements Serializable {
    private static final long serialVersionUID = -7315567579159181972L;

    private Long id;
    private String name;

    public CountryDTO(Country country) {
        this.id = country.getId();
        this.name = country.getName();
    }

    public Country toCountry(){
        Country country = new Country();
        country.setId(this.getId());
        country.setName(this.getName());

        return country;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
