package com.pearls.infoclients.controller;

import com.pearls.infoclients.dto.VisitDTO;
import com.pearls.infoclients.service.VisitService;
import com.pearls.infoclients.util.InsertVisitException;
import com.pearls.infoclients.util.RestResponse;
import com.pearls.infoclients.util.UpdateVisitException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/visit")
@CrossOrigin
public class VisitController {

    @Autowired
    VisitService visitService;

    @GetMapping("/{idClient}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public List<VisitDTO> findByClientId(@PathVariable Long idClient){
        return visitService.findByClientId(idClient);
    }

    @GetMapping("/groupbycity")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public List<VisitDTO> findAllGroupByCityName(){
        return visitService.findAllGroupByCityName();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    public RestResponse insert(@RequestBody VisitDTO visitDTO){
        RestResponse restResponse = null;
        try{
            visitService.insert(visitDTO);
            restResponse = new RestResponse(HttpStatus.OK.value(), "The visit was created");
        }catch(InsertVisitException ex){
            restResponse = new RestResponse(HttpStatus.NOT_ACCEPTABLE.value(), ex.getMessage());
        }

        return restResponse;
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public RestResponse update(@RequestBody VisitDTO visitDTO, @PathVariable Long id){
        RestResponse restResponse = null;
        try{
            visitService.update(visitDTO, id);
            restResponse = new RestResponse(HttpStatus.OK.value(), "The visit was updated");
        }catch(UpdateVisitException ex){
            restResponse = new RestResponse(HttpStatus.NOT_ACCEPTABLE.value(), ex.getMessage());
        }

        return restResponse;
    }
}
