package com.pearls.infoclients.controller;

import com.pearls.infoclients.dto.ClientDTO;
import com.pearls.infoclients.dto.PersonDTO;
import com.pearls.infoclients.dto.VisitDTO;
import com.pearls.infoclients.service.ClientService;
import com.pearls.infoclients.service.PersonService;
import com.pearls.infoclients.service.VisitService;
import com.pearls.infoclients.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/client")
@CrossOrigin
public class ClientController {

    @Autowired
    protected ClientService clientService;

    @Autowired
    protected PersonService personService;

    @Autowired
    protected VisitService visitService;

    @GetMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    public List<ClientDTO> findAll(){
        return clientService.findAll();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public RestResponse insert(@RequestBody ClientDTO clientDTO){
        PersonDTO personDTO = personService.findByID(clientDTO.getPersonDTO().getIdPerson());
        RestResponse restResponse = null;

        if(Objects.isNull(personDTO)){
            personDTO = clientDTO.getPersonDTO();
            try {
                personDTO = personService.insert(personDTO);
            } catch(InsertPersonException ex){
                restResponse = new RestResponse(HttpStatus.NOT_ACCEPTABLE.value(), ex.getMessage());
            }
        } else {
            try {
                personDTO = clientDTO.getPersonDTO();
                personDTO = personService.update(personDTO, personDTO.getIdPerson());
            } catch(UpdatePersonException ex){
                restResponse = new RestResponse(HttpStatus.NOT_ACCEPTABLE.value(), ex.getMessage());
            }
        }

        if(Objects.isNull(restResponse)) {
            clientDTO.setPersonDTO(personDTO);

            try {
                clientService.insert(clientDTO);
                /*List<VisitDTO> visitDTOList = clientDTO.getVisitDTOList();
                visitDTOList.forEach( e -> {
                    Long availableCredit = e.getClientDTO().getAvailableCredit();
                    availableCredit -= e.getVisitTotal();
                    e.getClientDTO().setAvailableCredit(availableCredit);
                });
                visitService.saveAll(visitDTOList);*/
                restResponse = new RestResponse(HttpStatus.OK.value(), "The client was created");
            } catch (InsertClientException ex) {
                restResponse = new RestResponse(HttpStatus.NOT_ACCEPTABLE.value(), ex.getMessage());
            }
        }

        return restResponse;
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public RestResponse update(@RequestBody ClientDTO clientDTO, @PathVariable Long id){
        PersonDTO personDTO = personService.findByID(clientDTO.getPersonDTO().getIdPerson());
        RestResponse restResponse = null;

        if(Objects.isNull(personDTO)){
            restResponse = new RestResponse(HttpStatus.NOT_ACCEPTABLE.value(), "Client doesn't exist in our Person database");
        } else {
            try {
                personDTO = clientDTO.getPersonDTO();
                personDTO = personService.update(personDTO, personDTO.getIdPerson());
            } catch(UpdatePersonException ex){
                restResponse = new RestResponse(HttpStatus.NOT_ACCEPTABLE.value(), ex.getMessage());
            }
        }

        if(Objects.isNull(restResponse)) {
            clientDTO.setPersonDTO(personDTO);

            try {
                clientService.update(clientDTO, id);
                List<VisitDTO> visitDTOList = clientDTO.getVisitDTOList();
                visitDTOList.forEach( e -> {
                    Long availableCredit = e.getClientDTO().getAvailableCredit();
                    availableCredit -= e.getVisitTotal();
                    e.getClientDTO().setAvailableCredit(availableCredit);
                });
                visitService.saveAll(visitDTOList);
                restResponse = new RestResponse(HttpStatus.OK.value(), "The client was created");
            } catch (UpdateVisitException | UpdateClientException ex) {
                restResponse = new RestResponse(HttpStatus.NOT_ACCEPTABLE.value(), ex.getMessage());
            }
        }

        return restResponse;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public RestResponse delete(@PathVariable Long id){
        RestResponse restResponse = null;

        List<VisitDTO> visitDTOList = visitService.findByClientId(id);

        try{
            visitService.deleteAll(visitDTOList);
            clientService.delete(id);
            restResponse = new RestResponse(HttpStatus.OK.value(), "The client was deleted");
        }catch(DeleteVisitException | DeleteClientException ex){
            restResponse = new RestResponse(HttpStatus.NOT_ACCEPTABLE.value(), ex.getMessage());
        }

        return restResponse;
    }
}
