package com.pearls.infoclients.controller;

import com.pearls.infoclients.dto.SalesRepresentativeDTO;
import com.pearls.infoclients.service.SalesRepresentativeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/salesrepresentative")
@CrossOrigin
public class SalesRepresentativeController {

    @Autowired
    SalesRepresentativeService service;

    @GetMapping("/{idCity}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public List<SalesRepresentativeDTO> findAll(@PathVariable Long idCity){
        return service.findByCityId(idCity);
    }
}
