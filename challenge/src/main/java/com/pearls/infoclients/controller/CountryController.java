package com.pearls.infoclients.controller;

import com.pearls.infoclients.dto.CountryDTO;
import com.pearls.infoclients.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/country")
@CrossOrigin
public class CountryController {

    @Autowired
    CountryService service;

    @GetMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    public List<CountryDTO> findAll(){
        return service.findAll();
    }
}
