package com.pearls.infoclients.controller;

import com.pearls.infoclients.dto.CityDTO;
import com.pearls.infoclients.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/city")
@CrossOrigin
public class CityController {

    @Autowired
    CityService service;

    @GetMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    public List<CityDTO> findAll(){
        return service.findAll();
    }
}
