package com.pearls.infoclients.controller;

import com.pearls.infoclients.dto.StateDTO;
import com.pearls.infoclients.service.StateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/state")
@CrossOrigin
public class StateController {

    @Autowired
    StateService service;

    @GetMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    public List<StateDTO> findAll(){
        return service.findAll();
    }
}
