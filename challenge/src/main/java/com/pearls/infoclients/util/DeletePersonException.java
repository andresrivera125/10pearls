package com.pearls.infoclients.util;

public class DeletePersonException extends Exception {

    private static final long serialVersionUID = 2918630769504757072L;

    public DeletePersonException() {
        super("It was a problem while we try to delete the basic information of the person.  Please contact the administrator");

    }
}
