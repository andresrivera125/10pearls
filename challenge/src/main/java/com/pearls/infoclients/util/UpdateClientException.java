package com.pearls.infoclients.util;

public class UpdateClientException extends Exception {

    private static final long serialVersionUID = 2918630769504757072L;

    public UpdateClientException() {
        super("It was a problem while we try to update the client's information.  Please contact the administrator");

    }
}
