package com.pearls.infoclients.util;

public class UpdatePersonException extends Exception {

    private static final long serialVersionUID = 2918630769504757072L;

    public UpdatePersonException() {
        super("It was a problem while we try to update the basic information of the person.  Please contact the administrator");

    }
}
