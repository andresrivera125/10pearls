package com.pearls.infoclients.util;

public class DeleteVisitException extends Exception {

    private static final long serialVersionUID = 2918630769504757072L;

    public DeleteVisitException() {
        super("It was a problem while we try to delete the visit.  Please contact the administrator");

    }
}
