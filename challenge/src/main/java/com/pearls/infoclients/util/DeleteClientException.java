package com.pearls.infoclients.util;

public class DeleteClientException extends Exception {

    private static final long serialVersionUID = 2918630769504757072L;

    public DeleteClientException() {
        super("It was a problem while we try to delete the client.  Please contact the administrator");

    }
}
