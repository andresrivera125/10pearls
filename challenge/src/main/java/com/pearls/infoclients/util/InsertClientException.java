package com.pearls.infoclients.util;

public class InsertClientException extends Exception {

    private static final long serialVersionUID = 2918630769504757072L;

    public InsertClientException() {
        super("It was a problem while we try to create the client's information.  Please contact the administrator");

    }
}
