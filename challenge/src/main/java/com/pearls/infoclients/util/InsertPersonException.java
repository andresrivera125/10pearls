package com.pearls.infoclients.util;

public class InsertPersonException extends Exception {

    private static final long serialVersionUID = 2918630769504757072L;

    public InsertPersonException() {
        super("It was a problem while we try to create the basic information of the person.  Please contact the administrator");

    }
}
